from main import *
import warnings

warnings.filterwarnings('ignore', category=FutureWarning, module='botocore.client')
logger = logging.getLogger()
logging.basicConfig()
logging.root.setLevel(logging.NOTSET)
logger.setLevel(logging.INFO)
ReadXMLSendEmail()