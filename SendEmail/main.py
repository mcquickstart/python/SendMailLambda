import json
import logging
import time
from HelperPackage.EvenBridgeOperations import *
from HelperPackage.S3Operations import *
from HelperPackage.SESOperations import *
from HelperPackage.SQSQueue import *
from HelperPackage.Helper import *

def ReadXMLSendEmail():
    #Set loggers
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    logger.info("In function: main.ReadXMLSendEmail")

    #Read Environment vaariables
    environment_variables = ReadEnvironmentVariables()
    xmlFileName=""

    #Check the queue size
    queue_size = GetQueueSize(environment_variables['queue_url'])

    #If the queue is emptyy then exit
    if queue_size["total_message_count"] == 0:
        logger.info("No messagge in Queue. Exiting function...")
        return '200'

    #If the queue is not empty and the number of visible messages are 0 then check if there is any inflight messages
    #If there are inflight messages then wait for timeout and calculte queue size again.
    if queue_size["visible_message_count"] == 0:
        if queue_size["inflight_message_count"] > 0 or queue_size["delayed_message_count"] > 0:
            logger.info("In flight messages found in Queue. No of messages found {}. Waiting for {} seconds to clear out the inflight messages".format(queue_size["inflight_message_count"], environment_variables['queue_url']))
            time.sleep(environment_variables['visibility_timeout'])
            queue_size = GetQueueSize(environment_variables['queue_url'])

    #Start getting the messages from the queue if there are visible items in the queue.
    if queue_size["visible_message_count"] > 0:

        #Get the messages from the SQS queue.
        sqs_meessages = GetMessages(
            queue_url= environment_variables['queue_url'], 
            max_number_of_messages = environment_variables['max_number_of_messages'],  
            visibility_timeout = environment_variables['visibility_timeout'], 
            wait_in_seconds = environment_variables['wait_time_in_seconds']
        )
        
        # If messages are fetched then loop through each message
        if sqs_meessages['message_count'] > 0:
            logger.info("Message count".format(sqs_meessages['message_count']))
            #Loop through all the messsages
            for message in sqs_meessages['messages']:
                try:
                    #Read the message
                    payload = json.loads(message['Body'])
                    bucket = environment_variables['bucket_name']
                    # Get the XML file name from the message
                    xmlFileName = payload['Records'][0]['s3']['object']['key']
                    #Read the XML file from the bucket
                    xmlData = ReadXMLFile(bucket, xmlFileName)
                    #load the email metadata from XML
                    emailMetadata = ReadEmailMetadata(
                        email_metadata= xmlData,
                        separator= environment_variables['separator']
                    )

                    #Download the attachment if present
                    if emailMetadata['has_attachement']:
                        DownloadFile(
                            bucket= environment_variables['bucket_name'],
                            prefix=environment_variables['prefix'],
                            key= emailMetadata['attachment'],
                            temp_path= environment_variables['attachment_file_location']
                        )
                    #Prepare the message to be sent.
                    message_content= PrepareMessage(
                        email_metadata=emailMetadata,
                        attachment_file="{}/{}".format(environment_variables['attachment_file_location'], emailMetadata['attachment'])
                    )
                    #Send the maiil. If the mail is sent succesfully then delete the XML, attachment and queue entry
                    if SendEmail(
                        mail_from= emailMetadata['from'],
                        mail_destinations=emailMetadata['destinations'],
                        message_as_string=message_content
                    ):
                        #Delete XML file
                        try:
                            DeleteObject(
                                bucketName=environment_variables['bucket_name'],
                                keyName=xmlFileName
                            )

                            #Delete the attachment file
                            DeleteObject(
                                bucketName=environment_variables['bucket_name'],
                                keyName=emailMetadata["attachment"]
                            )

                            #Delete the message from the queue
                            DeleteMessage(
                                queue_url=environment_variables['queue_url'],
                                receipt_handle=message['ReceiptHandle']
                            )
                        except Exception as e:
                            print(e)
                            logger.error("Failed to clean up.")

                except Exception as e:
                    print(e)
                    logger.error("Failed to send the message. XML file = {}.".format(xmlFileName))
        else:
            #No messagges are retrieved.Check the queue size again
            logger.info("Received empty memssage - chcking queue size.")
            queue_size = GetQueueSize(environment_variables['queue_url'])
            #If the queue is empty then exit.
            if queue_size["total_message_count"] == 0:
                logger.info("No messagge in Queue. Exiting function...")
                return 200
            #Else if there are inflight messages then wait till the visibility timeout occurs.
            elif queue_size["inflight_message_count"] > 0:
                logger.info("In flight messages found in Queue. No of messages found {}. Waiting for {} seconds to clear out the inflight messages".format(queue_size["inflight_message_count"], environment_variables['visibility_timeout']))
                time.sleep(environment_variables['visibility_timeout'])
    
    #Chck the queue size    
    queue_size = GetQueueSize(environment_variables['queue_url'])
    #Send an event to the EvevntBridge if the message count is not zero
    if queue_size["total_message_count"] > 0:
        logger.info("Queue is not empty. No of messages pendiding - {}. Sending event to EventBridge to trigger the function again.".format(queue_size["total_message_count"]))
        event_bus_event =SendEventToEventBridge(
            source=environment_variables['event_source'],
            resources=environment_variables['resources'],
            detail_type=environment_variables['detail_type'],
            detail=environment_variables['detail'],
            event_bus_name=environment_variables['event_bus_name']
        )

        if event_bus_event['FailedEntryCount'] == 0:
            logger.info("Event is successfully sent to EventBridge")
        else:
            logger.error("Failed to send the event to EventBridge")
    else:
        logger.info("The queue is empty. No need to send event.")
    
    return 200