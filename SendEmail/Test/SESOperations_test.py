import pytest
import sys
sys.path.append('F:\\\\Project\\\\SendEmailV2')
from HelperPackage import SESOperations
import xmltodict

def read_xml_file(xml_file):

    doc = None
    with open(xml_file) as fd:
        doc = xmltodict.parse(fd.read())
    return doc

@pytest.mark.parametrize("xml_file,metadata,field_name,separator", [
    (None,None,None,None),
    (None,None,None,""),
    (None,None,"",None),
    (None,"",None,None),
    (None,"",None,""),
    (None,"","",None),
    (None,"","",""),
    (None," ",None,None),
    (None," ",None,""),
    (None," ",None," "),
    (None," ","",None),
    (None," "," ",""),
    (None," "," "," "),
    (None,"This is a text.","This",True),
    (None,"This is a text.","This is"," "),
    ("F:\\Project\\SendEmailV2\\TestCases\\XML\\Sample1.xml",None,None,None),
    ("F:\\Project\\SendEmailV2\\TestCases\\XML\\Sample1.xml",None,"",None),
    ("F:\\Project\\SendEmailV2\\TestCases\\XML\\Sample1.xml",None,None,""),
    ("F:\\Project\\SendEmailV2\\TestCases\\XML\\Sample1.xml",None,None," "),
    ("F:\\Project\\SendEmailV2\\TestCases\\XML\\Sample1.xml",None," ",None),
    ("F:\\Project\\SendEmailV2\\TestCases\\XML\\Sample1.xml",None,"bccx",None),
    ("F:\\Project\\SendEmailV2\\TestCases\\XML\\Sample1.xml",None,"bccx",""),
    ("F:\\Project\\SendEmailV2\\TestCases\\XML\\Sample1.xml",None,"bccx"," "),
    ("F:\\Project\\SendEmailV2\\TestCases\\XML\\Sample1.xml",None,"bccx",";"),
    ("F:\\Project\\SendEmailV2\\TestCases\\XML\\Sample1.xml",None,"cc",None),
    ("F:\\Project\\SendEmailV2\\TestCases\\XML\\Sample1.xml",None,"cc",""),
    ("F:\\Project\\SendEmailV2\\TestCases\\XML\\Sample1.xml",None,"cc"," "),
    ("F:\\Project\\SendEmailV2\\TestCases\\XML\\Sample1.xml",None,"cc",";"),
])
def test_read_email_metadata_items_failure(xml_file, metadata, field_name, separator):

    if xml_file is not None:
        metadata = read_xml_file(xml_file)

    response = SESOperations.ReadEmailMetadataItems(
        metadata=metadata,
        field_name=field_name,
        separator=separator
    )
    
    assert response is None

@pytest.mark.parametrize("xml_file,metadata,field_name,separator", [
    ("F:\\Project\\SendEmailV2\\TestCases\\XML\\Sample1.xml",None,"to",None),
    ("F:\\Project\\SendEmailV2\\TestCases\\XML\\Sample1.xml",None,"to",""),
    ("F:\\Project\\SendEmailV2\\TestCases\\XML\\Sample1.xml",None,"to"," "),
    ("F:\\Project\\SendEmailV2\\TestCases\\XML\\Sample1.xml",None,"to",";"),
    ("F:\\Project\\SendEmailV2\\TestCases\\XML\\Sample1.xml",None,"bcc",None),
    ("F:\\Project\\SendEmailV2\\TestCases\\XML\\Sample1.xml",None,"bcc",""),
    ("F:\\Project\\SendEmailV2\\TestCases\\XML\\Sample1.xml",None,"bcc"," "),
    ("F:\\Project\\SendEmailV2\\TestCases\\XML\\Sample1.xml",None,"bcc",";")
])
def test_read_email_metadata_items_success(xml_file, metadata, field_name, separator):

    if xml_file is not None:
        metadata = read_xml_file(xml_file)

    response = SESOperations.ReadEmailMetadataItems(
        metadata=metadata,
        field_name=field_name,
        separator=separator
    )
    
    assert response is not None


@pytest.mark.parametrize("xml_file, metadata, separator",[
    (None,None,None),
    (None,None,""),
    (None,"",None),
    (None,"",""),
    (None,"",None),
    (None,"",""),
    (None," ",None),
    (None," ",""),
    (None," "," "),
    (None,"This is a text.",True),
    (None,"This is a text.",1),
    ("F:\\Project\\SendEmailV2\\TestCases\\XML\\Sample2.xml",None,";"),
    ("F:\\Project\\SendEmailV2\\TestCases\\XML\\Sample3.xml",None,";"),
    ("F:\\Project\\SendEmailV2\\TestCases\\XML\\Sample4.xml",None,";"),
    ("F:\\Project\\SendEmailV2\\TestCases\\XML\\Sample6.xml",None,";"),
])
def test_read_email_metadata_value_exceptions_to_blank(xml_file, metadata, separator):
    
    if xml_file is not None:
        metadata = read_xml_file(xml_file)
    
    response = None
    with pytest.raises(ValueError,  match=r"To .*"):
        response = SESOperations.ReadEmailMetadata(metadata, separator)

@pytest.mark.parametrize("xml_file, metadata, separator",[
    ("F:\\Project\\SendEmailV2\\TestCases\\XML\\Sample5.xml",None,";"),
])
def test_read_email_metadata_value_exceptions_subject_blank(xml_file, metadata, separator):
    
    if xml_file is not None:
        metadata = read_xml_file(xml_file)
    
    response = None
    with pytest.raises(ValueError,  match=r"Subject .*"):
        response = SESOperations.ReadEmailMetadata(metadata, separator)

@pytest.mark.parametrize("xml_file, metadata, separator",[
    ("F:\\Project\\SendEmailV2\\TestCases\\XML\\Sample7.xml",None,None),
    ("F:\\Project\\SendEmailV2\\TestCases\\XML\\Sample7.xml",None,""),
    ("F:\\Project\\SendEmailV2\\TestCases\\XML\\Sample7.xml",None,";"),
])
def test_read_email_metadata_value_success(xml_file, metadata, separator):
    
    if xml_file is not None:
        metadata = read_xml_file(xml_file)
    
    response = SESOperations.ReadEmailMetadata(metadata, separator)
    assert response is not None
    assert len(response['to']) > 0
    assert len(response['cc']) > 0
    assert len(response['bcc']) > 0
    assert len(response['from']) > 0
    assert len(response['subject']) > 0
    assert len(response['body']) > 0
    assert len(response['attachment']) > 0
    if(len(response['attachment']) > 0):
        assert response['has_attachement'] is True
    else:
        assert response['has_attachement'] is False

@pytest.mark.parametrize("email_metadata, attachment_file",  [
    (None,None),
    (None,""),
    (None," "),
    (None,"F:\\Project\\SendEmailV2\\TestCases\\Attachment\\Sample2.pdf"),
    (None,True),
    (None,"1"),
    ("",None),
    ("",""),
    (""," "),
    ("","F:\\Project\\SendEmailV2\\TestCases\\Attachment\\Sample2.pdf"),
    ("",True),
    ("","1"),
    (" ",None),
    (" ",""),
    (" "," "),
    (" ","F:\\Project\\SendEmailV2\\TestCases\\Attachment\\Sample2.pdf"),
    (" ",True),
    (" ","1"),
    ("This is a test",None),
    ("This is a test",""),
    ("This is a test"," "),
    ("This is a test","F:\\Project\\SendEmailV2\\TestCases\\Attachment\\Sample2.pdf"),
    ("This is a test",True),
    ("This is a test","1"),
])   
def test_prepare_message_failures_invalid_inputs(email_metadata, attachment_file):

    try:
        response = SESOperations.PrepareMessage(
            email_metadata=email_metadata,
            attachment_file=attachment_file
        )
        assert response is None
    except Exception as e:
         assert False, f"'PrepareMessage' raised an exception {e}"
    
