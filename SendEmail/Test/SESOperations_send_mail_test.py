import pytest
import sys
sys.path.append('F:\\\\Project\\\\SendEmailV2')
from HelperPackage import SESOperations
from time import sleep

metadata_all_valid_values_attachment = {
    "to" : ["rajsekhar.roy@outlook.com"],
    "cc" : ["rajsekhar.roy@outlook.com"],
    "bcc" : ["rajsekhar.roy@outlook.com"],
    "destinations" : ["rajsekhar.roy@outlook.com"],
    "from": "rajsekhar.roy@outlook.com",
    "subject": "This is a test subject 1.",
    "body" : "This is a test body 2.",
    "attachment":"Sample2.pdf",
    "has_attachement": True
}

metadata_all_valid_values_no_attachments = {
    "to" : ["rajsekhar.roy@outlook.com"],
    "cc" : ["rajsekhar.roy@outlook.com"],
    "bcc" : ["rajsekhar.roy@outlook.com"],
    "destinations" : ["rajsekhar.roy@outlook.com"],
    "from": "rajsekhar.roy@outlook.com",
    "subject": "This is a test subject 2.",
    "body" : "This is a test body 2.",
    "attachment":"",
    "has_attachement": False
}

metadata_all_valid_values_attachment_multiple = {
    "to" : ["rajsekhar.roy@outlook.com", "rajsekhar.roy@outlook.com"],
    "cc" : ["rajsekhar.roy@outlook.com"],
    "bcc" : ["rajsekhar.roy@outlook.com"],
    "destinations" : ["rajsekhar.roy@outlook.com"],
    "from": "rajsekhar.roy@outlook.com",
    "subject": "This is a test subject 1.",
    "body" : "This is a test body 2.",
    "attachment":"Sample2.pdf",
    "has_attachement": True
}

metadata_all_valid_values_no_attachments_multiple = {
    "to" : ["rajsekhar.roy@outlook.com", "rajsekhar.roy@outlook.com"],
    "cc" : ["rajsekhar.roy@outlook.com"],
    "bcc" : ["rajsekhar.roy@outlook.com"],
    "destinations" : ["rajsekhar.roy@outlook.com"],
    "from": "rajsekhar.roy@outlook.com",
    "subject": "This is a test subject 2.",
    "body" : "This is a test body 2.",
    "attachment":"",
    "has_attachement": False
}

def send_email_prepare_message(email_data, has_attachment):
    
    if not has_attachment:
        return SESOperations.PrepareMessage(
            email_metadata=email_data
        )
    else:
        return SESOperations.PrepareMessage(
            email_metadata=email_data,
            attachment_file="F:\\Project\\SendEmailV2\\TestCases\\Attachment\\Sample2.pdf"
        )

@pytest.mark.parametrize("mail_from, mail_destinations,message",[
    (None,None,None),
    (None,None,""),
    (None,"",None),
    (None,"",""),
    ("",None,None),
    ("",None,""),
    ("","",None),
    ("","",""),
    ("abc",None,None),
    ("abc",None,""),
    ("abc","",None),
    ("abc","",""),
    ("rajsekhar.roy@outlook.com",None,None),
    ("rajsekhar.roy@outlook.com",None,""),
    ("rajsekhar.roy@outlook.com","",None),
    ("rajsekhar.roy@outlook.com","",""),
    ("rajsekhar.roy@outlook.com","rajsekhar.roy@outlook.com",""),
    ("rajsekhar.roy@outlook.com","rajsekhar.roy@outlook.com",None),
    ("rajsekhar.roy@outlook.com","","This is a test"),
])
def test_send_email_invalid_inputs(mail_from, mail_destinations,message):
    response = SESOperations.SendEmail(
        mail_from=mail_from,
        mail_destinations=mail_destinations,
        message_as_string=message
    )
    assert response == False 
@pytest.mark.parametrize("input, has_attachment",[
    (metadata_all_valid_values_attachment, True),
    (metadata_all_valid_values_no_attachments,False),
    (metadata_all_valid_values_attachment_multiple,True),
    (metadata_all_valid_values_no_attachments_multiple,False)
])
def test_send_email_valid_inputs(input, has_attachment):
    sleep(2)
    email_data = send_email_prepare_message(input, has_attachment)
    response = SESOperations.SendEmail(
        mail_from=input['from'],
        mail_destinations=input['destinations'],
        message_as_string=email_data
    )

    assert response == True