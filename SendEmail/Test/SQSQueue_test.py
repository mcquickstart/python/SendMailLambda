import pytest
import sys
import warnings
import boto3

sys.path.append('F:\\Project\\SendEmailV2')

from HelperPackage import SQSQueue

@pytest.mark.parametrize("queue_url, expected_success", [
    ("None",False),
    ("",False),
    (" ",False),
    ("InvalidQueueName",False),
    ("https://sqs.ap-south-1.amazonaws.com/981340723481/EmailInfo",True)
])
def test_get_queue_size(queue_url, expected_success):
    warnings.filterwarnings('ignore', category=FutureWarning, module='botocore.client')
    
    response = SQSQueue.GetQueueSize(queue_url)

    assert response is not None
    assert response['success'] == expected_success
    if response['success']:
        assert response['visible_message_count'] > -1
        assert response['inflight_message_count'] > -1
        assert response['delayed_message_count'] > -1
        assert response['total_message_count'] > -1
    else:
        assert response['visible_message_count'] == -1
        assert response['inflight_message_count'] == -1
        assert response['delayed_message_count'] == -1
        assert response['total_message_count'] == -1

@pytest.mark.parametrize("queue_url, max_no_of_messages, visibility_timeout, wait_in_secocnds, message_count", [
    ("None",None,None,None,-1),
    ("",None,None,None,-1),
    ("None","",None,None,-1),
    ("None",None,"",None,-1),
    ("None",None,None,"",-1),
    ("","",None,None,-1),
    ("",None,"",None,-1),
    ("",None,None,"",-1),
    ("","","",None,-1),
    ("","",None,"",-1),
    (" "," "," "," ",-1),
    ("","","","",-1),
    ("InvalidQueueName",5,10,5,-1),
    ("1",5,10,5,-1),
    ("True",5,10,5,-1),
    ("InvalidQueueName",-1,10,5,-1),
    ("InvalidQueueName",0,10,5,-1),
    ("InvalidQueueName",1,10,5,-1),
    ("InvalidQueueName",9,10,5,-1),
    ("InvalidQueueName",10,10,5,-1),
    ("InvalidQueueName",11,10,5,-1),
    ("InvalidQueueName",20,10,5,-1),
    ("InvalidQueueName","Test",10,5,-1),
    ("InvalidQueueName",True,10,5,-1),
    ("InvalidQueueName",1.5,10,5,-1),
    ("InvalidQueueName",1,-1,5,-1),
    ("InvalidQueueName",1,0,5,-1),
    ("InvalidQueueName",1,1,5,-1),
    ("InvalidQueueName",1,120,5,-1),
    ("InvalidQueueName",1,43201,5,-1),
    ("InvalidQueueName",1,50000,5,-1),
    ("InvalidQueueName",1,"Test","",-1),
    ("InvalidQueueName",1,True,"",-1),
    ("InvalidQueueName",1,1.5,"",-1),
    ("InvalidQueueName",1,10,-1,-1),
    ("InvalidQueueName",1,10,0,-1),
    ("InvalidQueueName",1,10,1,-1),
    ("InvalidQueueName",1,10,20,-1),
    ("InvalidQueueName",1,10,21,-1),
    ("InvalidQueueName",1,10,50,-1),
    ("InvalidQueueName",1,10,"Test",-1),
    ("InvalidQueueName",1,10,True,-1),
    ("InvalidQueueName",1,10,12.5,-1),
    ("InvalidQueueName","","","",-1),
    ("https://sqs.ap-south-1.amazonaws.com/981340723481/EmailInfo",-1,10,5,-1),
    ("https://sqs.ap-south-1.amazonaws.com/981340723481/EmailInfo",0,10,5,-1),
    ("https://sqs.ap-south-1.amazonaws.com/981340723481/EmailInfo",1,10,5,0),
    ("https://sqs.ap-south-1.amazonaws.com/981340723481/EmailInfo",9,10,5,0),
    ("https://sqs.ap-south-1.amazonaws.com/981340723481/EmailInfo",10,10,5,0),
    ("https://sqs.ap-south-1.amazonaws.com/981340723481/EmailInfo",11,10,5,-1),
    ("https://sqs.ap-south-1.amazonaws.com/981340723481/EmailInfo",20,10,5,-1),
    ("https://sqs.ap-south-1.amazonaws.com/981340723481/EmailInfo","Test",10,5,-1),
    ("https://sqs.ap-south-1.amazonaws.com/981340723481/EmailInfo",True,10,5,-1),
    ("https://sqs.ap-south-1.amazonaws.com/981340723481/EmailInfo",1.5,10,5,-1),
    ("https://sqs.ap-south-1.amazonaws.com/981340723481/EmailInfo",1,-1,5,-1),
    ("https://sqs.ap-south-1.amazonaws.com/981340723481/EmailInfo",1,0,5,-1),
    ("https://sqs.ap-south-1.amazonaws.com/981340723481/EmailInfo",1,1,5,0),
    ("https://sqs.ap-south-1.amazonaws.com/981340723481/EmailInfo",1,120,5,0),
    ("https://sqs.ap-south-1.amazonaws.com/981340723481/EmailInfo",1,43201,5,-1),
    ("https://sqs.ap-south-1.amazonaws.com/981340723481/EmailInfo",1,50000,5,-1),
    ("https://sqs.ap-south-1.amazonaws.com/981340723481/EmailInfo",1,"Test","",-1),
    ("https://sqs.ap-south-1.amazonaws.com/981340723481/EmailInfo",1,True,"",-1),
    ("https://sqs.ap-south-1.amazonaws.com/981340723481/EmailInfo",1,1.5,"",-1),
    ("https://sqs.ap-south-1.amazonaws.com/981340723481/EmailInfo",1,10,-1,-1),
    ("https://sqs.ap-south-1.amazonaws.com/981340723481/EmailInfo",1,10,0,0),
    ("https://sqs.ap-south-1.amazonaws.com/981340723481/EmailInfo",1,10,1,0),
    ("https://sqs.ap-south-1.amazonaws.com/981340723481/EmailInfo",1,10,20,0),
    ("https://sqs.ap-south-1.amazonaws.com/981340723481/EmailInfo",1,10,21,-1),
    ("https://sqs.ap-south-1.amazonaws.com/981340723481/EmailInfo",1,10,50,-1),
    ("https://sqs.ap-south-1.amazonaws.com/981340723481/EmailInfo",1,10,"Test",-1),
    ("https://sqs.ap-south-1.amazonaws.com/981340723481/EmailInfo",1,10,True,-1),
    ("https://sqs.ap-south-1.amazonaws.com/981340723481/EmailInfo",1,10,12.5,-1)
])
def test_get_messages(queue_url, max_no_of_messages, visibility_timeout, wait_in_secocnds, message_count):
    warnings.filterwarnings('ignore', category=FutureWarning, module='botocore.client')
    
    if message_count >= 0 :
        send_messsage(queue_url)

    response = SQSQueue.GetMessages(
        queue_url=queue_url,
        max_number_of_messages=max_no_of_messages,
        visibility_timeout=visibility_timeout,
        wait_in_seconds=wait_in_secocnds
    )

    assert response is not None
    assert response['message_count'] >=  message_count
    if response['message_count'] > 0:
        assert len(response['messages']) == response['message_count']
    elif response['message_count'] == 0:
        assert response['message'] is not None

@pytest.mark.parametrize("queue_url, expected_result", [
    (None,False),
    ("",False),
    (" ",False),
    ("https://sqs.ap-south-1.amazonaws.com/981340723481/EmailInfo",True)
])
def test_delete_message(queue_url, expected_result):
    warnings.filterwarnings('ignore', category=FutureWarning, module='botocore.client')
    
    send_messsage(queue_url)

    messages = SQSQueue.GetMessages(
        queue_url=queue_url,
        max_number_of_messages=1,
        visibility_timeout=10,
        wait_in_seconds=1
        )
        
    if messages['message_count'] > 0:
        for message in messages['messages']:
            assert message is not None
            if message is not None:
                response = SQSQueue.DeleteMessage(queue_url, message['ReceiptHandle'])
                assert response is not None
                if response is not None:
                    assert type(response) is dict
                    if type(response) is dict:
                        assert 'success' in response
                        if 'success' in response:
                            assert response['success'] == expected_result
                        assert 'reason' in response
                        if 'reason' in response:
                            if response['success'] == True:
                                assert len(response['reason']) == 0
                            else:
                                assert len(response['reason']) > 0

def send_messsage(queue_url):
    warnings.filterwarnings('ignore', category=FutureWarning, module='botocore.client')
    queue_size = SQSQueue.GetQueueSize(queue_url)

    if queue_size['visible_message_count'] == 0:
        sqs = boto3.client('sqs')
        sqs.send_message(
            QueueUrl = queue_url,
            MessageBody = "This is a message."
        )