import pytest
import sys
sys.path.append('F:\\\\Project\\\\SendEmailV2')
from HelperPackage import SESOperations

metadata_empty = {}

metadata_empty_values = {
    "to" : [],
    "cc" : [],
    "bcc" : [],
    "destinations" : [],
    "from": "",
    "subject": "",
    "body" : "",
    "attachment":"",
    "has_attachement": True
}

metadata_to_empty_values = {
    "to" : [],
    "cc" : ["test@test.com"],
    "bcc" : ["test@test.com"],
    "destinations" : ["test@test.com"],
    "from": "test@test2.com",
    "subject": "This is a test subject",
    "body" : "This is a test body.",
    "attachment":"testbody.pdf",
    "has_attachement": True
}

metadata_to_blank_values = {
    "to" : [""],
    "cc" : ["test@test.com"],
    "bcc" : ["test@test.com"],
    "destinations" : ["test@test.com"],
    "from": "test@test2.com",
    "subject": "This is a test subject",
    "body" : "This is a test body.",
    "attachment":"testbody.pdf",
    "has_attachement": True
}

metadata_to_space_values = {
    "to" : [" "],
    "cc" : ["test@test.com"],
    "bcc" : ["test@test.com"],
    "destinations" : ["test@test.com"],
    "from": "test@test2.com",
    "subject": "This is a test subject",
    "body" : "This is a test body.",
    "attachment":"testbody.pdf",
    "has_attachement": True
}

metadata_subject_blank_values = {
    "to" : ["test@test.com"],
    "cc" : ["test@test.com"],
    "bcc" : ["test@test.com"],
    "destinations" : ["test@test.com"],
    "from": "test@test2.com",
    "subject": "",
    "body" : "This is a test body.",
    "attachment":"testbody.pdf",
    "has_attachement": True
}

metadata_subject_space_values = {
    "to" : ["test@test.com"],
    "cc" : ["test@test.com"],
    "bcc" : ["test@test.com"],
    "destinations" : ["test@test.com"],
    "from": "test@test2.com",
    "subject": " ",
    "body" : "This is a test body.",
    "attachment":"testbody.pdf",
    "has_attachement": True
}

metadata_no_attachment_has_attachment = {
    "to" : ["test@test.com"],
    "cc" : ["test@test.com"],
    "bcc" : ["test@test.com"],
    "destinations" : ["test@test.com"],
    "from": "test@test2.com",
    "subject": "This is a test subject.",
    "body" : "This is a test body.",
    "attachment":"",
    "has_attachement": True
}

metadata_all_valid_values_no_attachments = {
    "to" : ["test@test.com"],
    "cc" : ["test@test.com"],
    "bcc" : ["test@test.com"],
    "destinations" : ["test@test.com"],
    "from": "test@test2.com",
    "subject": "This is a test subject.",
    "body" : "This is a test body.",
    "attachment":"",
    "has_attachement": True
}

metadata_all_valid_values_with_attachments = {
    "to" : ["test@test.com"],
    "cc" : ["test@test.com"],
    "bcc" : ["test@test.com"],
    "destinations" : ["test@test.com"],
    "from": "test@test2.com",
    "subject": "This is a test subject.",
    "body" : "This is a test body.",
    "attachment":"Sample2.pdf",
    "has_attachement": True
}

metadata_all_valid_values_subject_missing = {
    "to" : ["test@test.com"],
    "cc" : ["test@test.com"],
    "bcc" : ["test@test.com"],
    "destinations" : ["test@test.com"],
    "from": "test@test2.com",
    "body" : "This is a test body.",
    "attachment":"Sample2.pdf",
    "has_attachement": True
}

metadata_all_valid_values_body_missing = {
    "to" : ["test@test.com"],
    "cc" : ["test@test.com"],
    "bcc" : ["test@test.com"],
    "destinations" : ["test@test.com"],
    "from": "test@test2.com",
    "subject": "This is a test subject.",
    "attachment":"Sample2.pdf",
    "has_attachement": True
}

metadata_all_valid_values_from_missing = {
    "to" : ["test@test.com"],
    "cc" : ["test@test.com"],
    "bcc" : ["test@test.com"],
    "destinations" : ["test@test.com"],
    "subject": "This is a test subject.",
    "body" : "This is a test body.",
    "attachment":"Sample2.pdf",
    "has_attachement": True
}

metadata_all_valid_values_to_missing = {
    "cc" : ["test@test.com"],
    "bcc" : ["test@test.com"],
    "destinations" : ["test@test.com"],
    "from": "test@test2.com",
    "subject": "This is a test subject.",
    "body" : "This is a test body.",
    "attachment":"Sample2.pdf",
    "has_attachement": True
}

metadata_all_valid_values_cc_missing = {
    "to" : ["test@test.com"],
    "bcc" : ["test@test.com"],
    "destinations" : ["test@test.com"],
    "from": "test@test2.com",
    "subject": "This is a test subject.",
    "body" : "This is a test body.",
    "attachment":"Sample2.pdf",
    "has_attachement": True
}

metadata_all_valid_values_bcc_missing = {
    "to" : ["test@test.com"],
    "cc" : ["test@test.com"],
    "destinations" : ["test@test.com"],
    "from": "test@test2.com",
    "subject": "This is a test subject.",
    "body" : "This is a test body.",
    "attachment":"Sample2.pdf",
    "has_attachement": True
}

metadata_all_valid_values_attachments_missing = {
    "to" : ["test@test.com"],
    "cc" : ["test@test.com"],
    "bcc" : ["test@test.com"],
    "destinations" : ["test@test.com"],
    "from": "test@test2.com",
    "subject": "This is a test subject.",
    "body" : "This is a test body.",
    "has_attachement": False
}

metadata_all_valid_values_attachments_missing_invalid = {
    "to" : ["test@test.com"],
    "cc" : ["test@test.com"],
    "bcc" : ["test@test.com"],
    "destinations" : ["test@test.com"],
    "from": "test@test2.com",
    "subject": "This is a test subject.",
    "body" : "This is a test body.",
    "has_attachement": True
}

@pytest.mark.parametrize("email_metadata", [
    metadata_empty,
    metadata_all_valid_values_subject_missing,
    metadata_all_valid_values_from_missing,
    metadata_all_valid_values_to_missing,
    metadata_no_attachment_has_attachment,
    metadata_all_valid_values_with_attachments,
    metadata_all_valid_values_cc_missing,
    metadata_all_valid_values_bcc_missing,
    metadata_all_valid_values_body_missing,
    metadata_all_valid_values_attachments_missing_invalid,
    metadata_empty_values,
    metadata_to_empty_values,
    metadata_to_blank_values,
    metadata_to_space_values,
    metadata_subject_blank_values,
    metadata_subject_space_values,
    metadata_all_valid_values_no_attachments,
])
def test_prepare_message_dict_input_invalid_attachment_file_exceptions(email_metadata):
    with pytest.raises((ValueError, FileNotFoundError),  match=r".* cannot be blank|found"):
        response = SESOperations.PrepareMessage(
            email_metadata=email_metadata,
            attachment_file="F:\\Project\\SendEmailV2\\TestCases\\Attachment\\Sample.pdf"
        )

@pytest.mark.parametrize("email_metadata", [
    metadata_all_valid_values_attachments_missing
])
def test_prepare_message_dict_input_invalid_attachment_file(email_metadata):
    try:
        response = SESOperations.PrepareMessage(
            email_metadata=email_metadata,
            attachment_file="F:\\Project\\SendEmailV2\\TestCases\\Attachment\\Sample.pdf"

        )
        assert response is not None
        assert len(response) > 0
    except Exception as e:
         assert False, f"'PrepareMessage' raised an exception {e}"

