import pytest
import sys
sys.path.append('F:\\Project\\SendEmailV2')

from HelperPackage import S3Operations

@pytest.mark.parametrize("bucket_name, key, expected_value",[
    (None,None,False),
    (None,"",False),
    ("",None,False),
    ("","",False),
    (" "," ",False),
    ("Test",None,False),
    ("Test","",False),
    ("Test"," ",False),
    ("Test","Test",False),
    ("mcquickstart-dev-sendmail-demo",None,False),
    ("mcquickstart-dev-sendmail-demo","",False),
    ("mcquickstart-dev-sendmail-demo"," ",True),
    ("mcquickstart-dev-sendmail-demo","@",True),
    ("mcquickstart-dev-sendmail-demo","Sample3.pdf",True),
    ("mcquickstart-dev-sendmail-demo","PDF/Sample1.pdf",True),
    (True,"PDF/Sample2.pdf",False),
    (10,"PDF/Sample2.pdf",False),
    ("mcquickstart-dev-sendmail-demo",True,False),
    ("mcquickstart-dev-sendmail-demo",18,False)
])
def test_delete_object(bucket_name, key, expected_value):
    assert S3Operations.DeleteObject(
        bucketName=bucket_name,
        keyName=key
        ) == expected_value

@pytest.mark.parametrize("bucket_name, prefix, key, temp_path,  expected_value",[
    (None,None,None,None,False),
    ("",None,None,None,False),
    (None,"",None,None,False),
    (None,None,"",None,False),
    (None,None,"",None,False),
    ("","",None,None,False),
    ("",None,"",None,False),
    ("",None,None,"",False),
    ("","","",None,False),
    ("","",None,"",False),
    ("","","","",False),
    ("Test",None,None,None,False),
    ("Test","",None,None,False),
    ("Test",None,"",None,False),
    ("Test",None,None,"",False),
    ("Test","","",None,False),
    ("Test","",None,"",False),
    ("Test","","","",False),
    ("mcquickstart-dev-sendmail-demo",None,None,None,False),
    ("mcquickstart-dev-sendmail-demo","",None,None,False),
    ("mcquickstart-dev-sendmail-demo",None,"",None,False),
    ("mcquickstart-dev-sendmail-demo",None,None,"",False),
    ("mcquickstart-dev-sendmail-demo","","",None,False),
    ("mcquickstart-dev-sendmail-demo","",None,"",False),
    ("mcquickstart-dev-sendmail-demo","","","",False),
    ("mcquickstart-dev-sendmail-demo","True","False","False",False),
    ("mcquickstart-dev-sendmail-demo","Test","SomeKey.txt","SomeValue",False),
    ("mcquickstart-dev-sendmail-demo","PDF","Sample22.xml","SomeValue",False),
    ("mcquickstart-dev-sendmail-demo","PDF/","Sample2.pdf","SomeValue",False),
    ("mcquickstart-dev-sendmail-demo","PDF/","Sample2.pdf",".",True),
    ("mcquickstart-dev-sendmail-demo","PDF/","Sample2.pdf","../",True),
    ("mcquickstart-dev-sendmail-demo","PDF/","Sample2.pdf","F:\\Project\\SendEmailV2\\tmp",True),
    ("mcquickstart-dev-sendmail-demo",None,"Sample1.xml","F:\\Project\\SendEmailV2\\tmp",True)

])
def test_download_file(bucket_name, prefix, key, temp_path, expected_value):
    assert S3Operations.DownloadFile(
        bucket=bucket_name,
        prefix=prefix,
        key=key,
        temp_path=temp_path,
    ) == expected_value

@pytest.mark.parametrize("bucket_name, key, expected_value", [
    (None,None,False),
    (None,"",False),
    ("",None,False),
    ("","",False),
    (" "," ",False),
    ("Test",None,False),
    ("Test","",False),
    ("Test"," ",False),
    ("Test","Test",False),
    ("mcquickstart-dev-sendmail-demo",None,False),
    ("mcquickstart-dev-sendmail-demo","",False),
    ("mcquickstart-dev-sendmail-demo"," ",False),
    ("mcquickstart-dev-sendmail-demo","@",False),
    ("mcquickstart-dev-sendmail-demo","Sample3.pdf",False),
    ("mcquickstart-dev-sendmail-demo","PDF/Sample2.pdf",False),
    (True,"PDF/Sample2.pdf",False),
    (10,"PDF/Sample2.pdf",False),
    ("mcquickstart-dev-sendmail-demo",True,False),
    ("mcquickstart-dev-sendmail-demo",18,False),
    ("mcquickstart-dev-sendmail-demo","XML/Sample2.xml",True),

])
def test_read_xml_file(bucket_name, key, expected_value):
    response = S3Operations.ReadXMLFile(
        bucket=bucket_name,
        key=key
    )

    assert response['success'] == expected_value
    if  response['success'] == True:
        assert len(response['data']) > 0
    else:
        assert len(response['data']) == 0
