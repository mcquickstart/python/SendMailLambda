import logging
import os
import boto3
import os
def SendEventToEventBridge(source, resources, detail_type, detail, event_bus_name):
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    logger.info("In function: EvenBridgeOperations.SendEventToEventBridge")

    eventBridge = boto3.client('events')

    response = eventBridge.put_events(
        Entries=[
                {
                    'Source': source,
                    'Resources': [
                        resources
                    ],
                    'DetailType': detail_type,
                    'Detail': detail,
                    'EventBusName': event_bus_name
                }
            ]  
        )
        
    return response