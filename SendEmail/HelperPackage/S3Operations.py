import logging
import boto3
import xmltodict

def DeleteObject(bucketName, keyName):
    '''
    Description: This funcion delete the object specified.
    Inputs:
        Parameter   : bucketName
        Descritption: Specify the name of the bucket
        Data Type   : String

        Parameter   : keyName
        Descritption: Specify the key of the object
        Data Type   : String
    Outputs:
        Returns     : success
        Data Type   : Dictionary
    '''
    #Set the logger to log level information
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    logger.info("In function: S3Operations.DeleteObject")
    
    s3 = boto3.client('s3')
    try:
        s3.delete_object(Bucket=bucketName,Key=keyName)
        response = True
    except Exception as e:
        logger.error(e)
        logger.error('Unable to delete the file from the bucket')
        response = False
    
    return response

def DownloadFile(bucket, prefix, key, temp_path):
    #Set the logger to log level information
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    logger.info("In function: S3Operations.DownloadFile")

    s3 = boto3.client('s3')
    
    try:
        keyvalue = "{}{}".format(prefix, key) if prefix is not None and len(prefix) > 0 else key
        logger.info("File to download: {}".format(keyvalue))
        s3.download_file(
            Bucket=bucket,
            Key = keyvalue,
            Filename = "{}/{}".format(temp_path,key)
        )

        response = True
    except Exception as e:
        logger.error(e)
        logger.error('Unable to download file')
        response = False
    
    return response

def ReadXMLFile(bucket, key):
    #Set the logger to log level information
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    logger.info("In function: S3Operations.ReadXMLFile")
    
    s3 = boto3.client('s3')
    response = {}
    try:
        xmlContent = s3.get_object(Bucket=bucket, Key=key)
        logger.info('XML file is read successfully.')
        
        xmlBody = xmlContent['Body'].read().decode()
        xmlData_dict = xmltodict.parse(xmlBody)
        
        response = {
            "success": True,
            "data": xmlData_dict
        }
    except Exception as e:
        logger.error(e)
        logger.error('Error getting object {} from bucket {}. Make sure they exist and your bucket is in the same region as this function.'.format(key, bucket))
        response = {
            "success": False,
            "data":[]
        } 
    
    return response
    


