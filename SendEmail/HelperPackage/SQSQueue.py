import logging
from urllib import response
import boto3

def GetQueueSize(queue_url):
    '''
    Description: This funcion accepts the queue URL and returns the approximate number of visible, inflight and delayed messages.
    Inputs:
        Parameter   : queue_url
        Descritption: Specify the url of the queue you want to check
        Data Type   : String
    Outputs:
        Returns     : success, visible_message_count, inflight_message_count, delayed_message_count and total_message_count
        Data Type   : Dictionary
    '''
    #Set the logger to log level information
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    logger.info("In function: SQSQueue.GetQueueSize")

    sqs =  boto3.client('sqs')

    logger.info("Get the queue attributes to get the total message count")
    try:
        queue_attr = sqs.get_queue_attributes(
            QueueUrl = queue_url,
            AttributeNames=['ApproximateNumberOfMessages', 'ApproximateNumberOfMessagesNotVisible', 'ApproximateNumberOfMessagesDelayed']
            )
        visible_message_count = int(queue_attr['Attributes']['ApproximateNumberOfMessages'])
        logger.info('Visible message count = {}'.format(visible_message_count))
        
        inflight_message_count = int(queue_attr['Attributes']['ApproximateNumberOfMessagesNotVisible'])
        logger.info('Infight message count = {}'.format(inflight_message_count))

        delayed_message_count = int(queue_attr['Attributes']['ApproximateNumberOfMessagesDelayed'])
        logger.info('Infight message count = {}'.format(delayed_message_count))
        
        total_message_count = visible_message_count + inflight_message_count + delayed_message_count
        logger.info('Total message count = {}'.format(total_message_count))
        
        response = {
            "success" : True,
            "visible_message_count" : visible_message_count,
            "inflight_message_count" : inflight_message_count,
            "delayed_message_count" : delayed_message_count,
            "total_message_count" : total_message_count
        }
    except Exception as e:
        logger.error('Unable to read from the queue at this moment')
        logger.error(e)
        response={
            "success" : False,
            "visible_message_count" : -1,
            "inflight_message_count" : -1,
            "delayed_message_count" : -1,
            "total_message_count" : -1
        }
    
    return response

def DeleteMessage(queue_url, receipt_handle):
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    logger.info("In function: SQSQueue.DeleteMessage")

    sqs =  boto3.client('sqs')
    response = {
        "success": False,
        "reason": ""
    }
    try:
        status = sqs.delete_message(
            QueueUrl = queue_url,
            ReceiptHandle = receipt_handle
        )
        response["success"] = True
    except Exception as e:
        logger.error('Unable to delete the message from the queue')
        logger.error(e)
        response["reason"] = "Exception occured"
        logger.error("Reason: {}".format(response["reason"]))

    
    return response

def GetMessages(queue_url : str, max_number_of_messages : int, visibility_timeout: int, wait_in_seconds: int):
    '''
    Description: This funcion reads a SQS  queue and returns messages.
    Inputs:
        Parameter   : queue_url
        Descritption: Specify the url of the queue from  which you want to delete the message
        Data Type   : String

        Parameter   : max_number_of_messages
        Descritption: Specify maximum number of messages to return.
        Data Type   : int

        Parameter   : visibility_timeout
        Descritption: Specify visibility timeout.
        Data Type   : int

        Parameter   : wait_in_seconds
        Descritption: Specify wait time in seconds.
        Data Type   : int
    Outputs:
        Returns     : message_count, messages
        Data Type   : Dictionary
    '''
    
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    logger.info("In function: SQSQueue.GetMessages")

    sqs =  boto3.client('sqs')

    try:
        logger.info("Receiving the messages from  the Queue.")
        messages = sqs.receive_message(
            QueueUrl = queue_url,
            MaxNumberOfMessages = max_number_of_messages,
            VisibilityTimeout=visibility_timeout,
            WaitTimeSeconds=wait_in_seconds
            )

        response = {
            "message_count": len(messages['Messages']),
            "messages":  messages['Messages']
        }
    except Exception as e: 
        print(e)
        logger.error('Unable to get the message from the queue')
        response = {
            "message_count" : -1,
            "messages": []
        }
    
    return response

