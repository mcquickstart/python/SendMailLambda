import logging
import os

def ReadEnvironmentVariables():

    #Set the logger to log level information
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    logger.info("In function: Helper.ReadEnvironmentVariables")

    response_object =  {
        "queue_url" : os.environ['PrjQueueURL'],
        "bucket_name" : os.environ['PrjBucketName'],
        "prefix" : os.environ['PrjObjectPrefix'],
        "countdown_timer" :float(os.environ['PrjRunTime']),
        "max_number_of_messages": int(os.environ['PrjMaximumNumberOfMessages']),
        "visibility_timeout": int(os.environ['PrjVisibilityTimeOut']),
        "wait_time_in_seconds": int(os.environ['PrjWaitTime']),
        "event_source": os.environ['PrjEventSource'],
        "resources": [os.environ['PrjResources']],
        "detail_type": os.environ['PrjDetailType'],
        "detail": os.environ['PrjDetail'],
        "event_bus_name": os.environ['PrjEventBusName'], 
        "attachment_file_location": os.environ['PrjAttachmentLocation'],
        "separator": os.environ['Prjseparator']
    }

    logger.info("Environment Variables: {}".format(response_object))
    return response_object