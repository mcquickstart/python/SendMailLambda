import logging
import boto3
from botocore.exceptions import ParamValidationError
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from os.path import exists
from os.path import basename

def SendEmail (mail_from, mail_destinations:list, message_as_string) :
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    logger.info("In function: SESOperations.SendEmail")

    ses = boto3.client('ses')

    try:
        response = ses.send_raw_email(
            Source=mail_from,
            Destinations= mail_destinations,
            RawMessage={ 'Data': message_as_string }
        )
        logger.info('Message id: {}'.format(response['MessageId']))
    except ParamValidationError as pve:
        print(pve.args[0])
        logger.error(pve.args[0])
        return False
    except Exception as e:
        print(e.response['Error']['Message'])
        logger.error(e.response['Error']['Message'])
        return False
    else:
        logger.info('Message is sent successfully.')
        return True

def ReadEmailMetadata(email_metadata, separator):
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    logger.info("In function: SESOperations.ReadEmailMetadata")

    response = {
        "to" : [],
        "cc" : [],
        "bcc" : [],
        "destinations" : [],
        "from": "",
        "subject": "",
        "body" : "",
        "attachment":"",
        "has_attachement": False
    }

    # To field can have multiple email ids separated by separator
    response["to"] = ReadEmailMetadataItems(email_metadata, "to", separator)
    logger.info("To field: {}".format((response["to"])))
    if response["to"] is None:
        logger.error("To field is blank")
        raise ValueError("To field cannot be blank")
    else:
        response["destinations"].append(response["cc"])
    
    logger.info("Destinations field: {}".format((response["destinations"])))

    # CC field can have multiple email ids separated by separator
    response["cc"] = ReadEmailMetadataItems(email_metadata, "cc", separator)
    logger.info("CC field: {}".format((response["cc"])))
    if response['cc'] is not None:
        response["destinations"].append(response["cc"])
    
    logger.info("Destinations field: {}".format((response["destinations"])))

    # BCC field can have multiple email ids separated by separator
    response["bcc"] = ReadEmailMetadataItems(email_metadata, "bcc", separator)
    logger.info("BCC field: {}".format((response["bcc"])))
    if response['bcc'] is not None:
        response["destinations"].append(response["bcc"])
    
    logger.info("Destinations field: {}".format((response["destinations"])))

    #From field
    response["from"] = ReadEmailMetadataItems(email_metadata, "from")
    logger.info("From field: {}".format((response["from"])))

    #Subject field
    response["subject"] = ReadEmailMetadataItems(email_metadata, "subject")
    logger.info("Subject field: {}".format((response["subject"])))
    if response["subject"] is None:
        logger.error("Subject is blank")
        raise ValueError("Subject cannot be blank")

    #Body field
    response["body"] = ReadEmailMetadataItems(email_metadata, "body")
    logger.info("Body field: {}".format((response["body"])))
    

    #From field
    response["attachment"] = ReadEmailMetadataItems(email_metadata, "attachment")
    logger.info("Attachment field: {}".format((response["attachment"])))


    if response["attachment"] is not None:
        response["has_attachement"]=True
    else:
        response["has_attachement"]=False

    return response

def ReadEmailMetadataItems(metadata, field_name, separator=None):
    try:
        if metadata['email'][field_name] is not None:
            if separator is not None and len(separator) > 0:
                return metadata['email'][field_name].split(separator)
            else:
                return metadata['email'][field_name]
        else: 
            return None
    except:
        return None

def PrepareMessage(email_metadata:dict, attachment_file:str= ""):
    #Set the logger to log level information
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    logger.info("In function: SESOperations.PrepareMessage")
    response_string = None

    if email_metadata is not None and type(email_metadata) is dict:
        msg = MIMEMultipart()
        if 'subject' in email_metadata:
            msg['Subject'] = email_metadata['subject']
        else:
            logger.error("Subject is blank")
            raise ValueError("Subject cannot be blank")
        if 'from' in email_metadata:
            msg['From'] = email_metadata['from']
        else:
            logger.error("From address is blank")
            raise ValueError("From address cannot be blank")

        if 'to' in email_metadata:
            msg['To'] = ', '.join(email_metadata['to'])
        else:
            logger.error("From address is blank")
            raise ValueError("To address cannot be blank")

        msg['Cc'] = ', '.join(email_metadata['cc']) if 'cc' in email_metadata and email_metadata['cc'] is not None else ', '
        msg['Bcc'] = ', '.join(email_metadata['bcc']) if 'bcc' in email_metadata and email_metadata['bcc'] is not None else ', '
        textpart = MIMEText(email_metadata['body']) if 'body' in email_metadata and email_metadata['body'] is not None else MIMEText('')
        msg.attach(textpart)
        if email_metadata['has_attachement']:
            if attachment_file is None or len(attachment_file) == 0:
                logger.error("Attachment file is not provided.")
                raise ValueError("Attachment file cannot be blank")
            if not exists(attachment_file):
                logger.error("Attachment file is invalid.")
                raise FileNotFoundError("Attachment file cannot be found")

            att = MIMEApplication(open(attachment_file, 'rb').read())
            att.add_header('Content-Disposition','attachment',filename=email_metadata['attachment'] if 'attachment' in email_metadata and email_metadata['attachment'] is not None else basename(attachment_file))
            msg.attach(att)
    
        response_string = msg.as_string()

    return response_string